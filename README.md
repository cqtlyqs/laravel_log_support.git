# laravel_log_support

#### 介绍

laravel日志组件，使用中间件+队列+注释方式，记录日志

#### 使用说明

1. 引入

```
composer require ktnw/log_support
```

2. 发布

```
php artisan vendor:publish --provider="Ktnw\LogSupport\Providers\LogSupportServiceProvider"  
```

3. 修改namespace

```
php artisan support:log
```

4. 配置env文件

```
QUEUE_SAVE_OP_LOG=save_logs
USER_OP_LOG_TABLE_NAME=user_op_log
```

其中，USER_OP_LOG_TABLE_NAME为用户操作日志表的表名；QUEUE_SAVE_OP_LOG为队列名称

5. 创建用户操作日志表<br>
   先根据实际业务需求扩展用户操作日志记录表后，再进行表迁移。

```
php artisan migrate --path database/migrations/2022_02_25_140244_create_user_op_log_table.php
```

6. 配置queue.php

```
主要配置queue和retry_after
若使用redis集群，注意 queue的 配置需加{}
示例如下:
'redis' => [
    'driver'      => 'redis',
    'connection'  => env('QUEUE_REDIS_CONNECTION', 'default'),
    'queue'       => env('REDIS_QUEUE', 'default'),
    // 定义任务在执行以后多少秒后释放回队列。如果retry_after 设定的值为 90, 任务在运行 90 秒后还未完成，那么将被释放回队列而不是删除掉。毫无疑问，你需要把 retry_after 的值设定为任务执行时间的最大可能值。
    'retry_after' => 3600, // 单位:秒
    'block_for'   => null,
],
```

7. 创建failed_jobs_table表
```
php artisan queue:failed-table
```
```
php artisan migrate --path database/migrations/2022_02_25_162627_create_failed_jobs_table.php
```
注意：表名可能会变，表迁移时，请根据实际情况进行.

8. App\Http\Kernel.php配置 <br>
   $routeMiddleware中增加如下配置
```
'save_log'           => \App\Http\Middleware\SaveLogMiddleware::class,
```
配置后，需要记录日志的路由中，使用中间件 save_log。
此种方式，可灵活配置，对于高并发的路由，可以不使用。

9. 根据实际业务修改SaveLogMiddleware <br>
主要设置: RESPONSE_SUCCESS_CODE.


10. 运行队列

```angular2html
php artisan queue:work  --queue=save_logs --tries=1
```

11. 在需要记录用户操作日志的controller方法上添加注释：@saveLog，可根据实际业务需求，扩展:SaveLogMiddleware.

```
/**
 *@saveLog opType:user-save,   opDesc:保存用户
 */
 public function save(){
     // TODO 
 }
 
注释参数说明：
opType：用户操作类型
opDesc：用户操作描述
注意：:(英文冒号)和 ,(英文逗号)为分割符号，opType和opDesc字符串中不要包含这个两个字符。

```